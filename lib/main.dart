import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view_app/screen/home/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: Color(0xFFFEFEFE),
        primaryColor: Color(0xFFF36F7C),
        accentColor: Color(0xFFFFF2F3),
      ),
      home: HomePage(),
    );
  }
}


