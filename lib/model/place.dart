class Place {
  String title;
  String subtitle;
  String imageUrl;
  double height;

  Place(this.title, this.subtitle, this.imageUrl, this.height);

  static List<Place> generatePlace() {
    return [
      Place(
        "Rain Forest", 
        "Costa Rica", 
        "assets/images/image01.png", 
        240,
      ),
      Place(
        "Lake Louise", 
        "Canada", 
        "assets/images/image02.png", 
        200,
      ),
      Place(
        "Plitivice Lakes", 
        "Canada", 
        "assets/images/image04.png", 
        120,
      ),
      Place(
        "Dubai", 
        "UAE", 
        "assets/images/image03.png", 
        200,
      ),
      Place(
        "Taj Mahal", 
        "India", 
        "assets/images/image05.png", 
        240,
      ),
      Place(
        "Raja Ampat", 
        "Indonesia", 
        "assets/images/image06.png", 
        150,
      ),
    ];
  }
}
