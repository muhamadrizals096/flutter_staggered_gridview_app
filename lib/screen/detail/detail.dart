import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view_app/model/place.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/widget/about.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/widget/book_button.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/widget/feature_list.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/widget/my_header.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/widget/place_name.dart';

class DetailPage extends StatelessWidget {
  final Place place;
  DetailPage(this.place);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            MyHeader(place.imageUrl),
            PlaceName(place.title, place.subtitle),
            About(),
            FeatureList(),
            BookButton(),
          ],
        ),
      ),
    );
  }
}
