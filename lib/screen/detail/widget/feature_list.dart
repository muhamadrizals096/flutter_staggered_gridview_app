import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FeatureList extends StatefulWidget {
  @override
  State<FeatureList> createState() => _FeatureListState();
}

class _FeatureListState extends State<FeatureList> {
  final featureList = ['bookmark', 'time', 'hotel', 'plane', 'share'];
  int currentSelect = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      padding: EdgeInsets.symmetric(horizontal: 25),
      height: 55,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  setState(() {
                    currentSelect = index;
                  });
                },
                child: Container(
                  width: 55,
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                    color: currentSelect == index
                        ? Theme.of(context).primaryColor
                        : Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      currentSelect == index
                          ? BoxShadow()
                          : BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 1,
                              blurRadius: 4,
                              offset: Offset(0, 2),
                            ),
                    ],
                  ),
                  child: SvgPicture.asset(
                    'assets/icons/${featureList[index]}.svg',
                    color: currentSelect == index
                        ? Colors.white
                        : Theme.of(context).primaryColor,
                  ),
                ),
              ),
          separatorBuilder: (_, index) => SizedBox(width: 17),
          itemCount: featureList.length),
    );
  }
}
