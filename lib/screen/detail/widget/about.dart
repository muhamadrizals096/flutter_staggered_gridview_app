import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'About',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Oki is a group of four inhabited and around 180 small uninhabited islands located in the Sea of Japan roughly 50km north of Shimane Peninsula, north-west of the main island Honshu. The four inhabited islands include the large and circular Dōgo (Okinoshima Town), and the three Dōzen Islands: Nishinoshima (Nishinoshima Town), Nakanoshima (Ama Town) and Chiburijima (Chibu Village) which form the Dōzen Caldera. Dōgo Island and the Dōzen Islands are separated by the Oki.',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 14,
              height: 1.2,
            ),
          ),
          SizedBox(height: 4),
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Theme.of(context).primaryColor,
                  width: 1,
                ),
              ),
            ),
            child: Text(
              'Read More',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
