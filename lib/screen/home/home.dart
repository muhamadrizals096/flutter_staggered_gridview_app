import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view_app/screen/home/widget/category_list.dart';
import 'package:flutter_staggered_grid_view_app/screen/home/widget/place_staggered_view.dart';
import 'package:flutter_staggered_grid_view_app/screen/home/widget/search_input.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SearchInput(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Text(
                'Explore the world\nwith us!',
                style: TextStyle(
                  fontFamily: 'Georgia',
                  height: 1.3,
                  fontSize: 24,
                ),
              ),
            ),
            CategoryList(),
            PlaceStaggeredGridView(),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Theme.of(context).backgroundColor,
      leading: IconButton(
        onPressed: () {},
        icon: SvgPicture.asset(
          'assets/icons/menu.svg',
          width: 24,
        ),
      ),
      actions: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 2),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Image.asset(
            'assets/images/user.png',
            width: 32,
          ),
        ),
      ],
    );
  }
}
