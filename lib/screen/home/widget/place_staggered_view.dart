import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_staggered_grid_view_app/model/place.dart';
import 'package:flutter_staggered_grid_view_app/screen/home/widget/place_item.dart';

class PlaceStaggeredGridView extends StatelessWidget {
  final placeList = Place.generatePlace();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: StaggeredGridView.countBuilder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        crossAxisSpacing: 14,
        mainAxisSpacing: 14,
        itemCount: placeList.length,
          crossAxisCount: 4,
          itemBuilder: (context, index) => PlaceItem(placeList[index]),
          staggeredTileBuilder: (_) => StaggeredTile.fit(2)),
    );
  }
}
