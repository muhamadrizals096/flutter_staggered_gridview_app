import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view_app/model/place.dart';
import 'package:flutter_staggered_grid_view_app/screen/detail/detail.dart';

class PlaceItem extends StatelessWidget {
  final Place place;
  PlaceItem(this.place);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => DetailPage(place))
        );
      },
      child: Stack(
        children: [
          Container(
            alignment: Alignment.bottomLeft,
            height: place.height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              image: DecorationImage(
                image: AssetImage(place.imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            height: place.height,
            alignment: Alignment.bottomLeft,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black.withAlpha(0),
                  Colors.black.withAlpha(0),
                  Colors.black.withAlpha(0),
                  Colors.black12,
                  Colors.black54,
                  Colors.black87,
                ],
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    place.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    place.subtitle,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
